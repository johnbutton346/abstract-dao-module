package ru.isys.jpa.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.isys.jpa.entity.BaseEntity;
import ru.isys.jpa.repository.AbstractRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.transaction.annotation.Propagation.REQUIRED;

@Slf4j
public abstract class AbstractServiceDao<E extends BaseEntity, R extends AbstractRepository<E>> extends AbstractDao<E, R> {

    protected final R repository;

    @Autowired
    public AbstractServiceDao(R repository) {
        super(repository);
        this.repository = repository;
    }

    @Transactional(propagation = REQUIRED, readOnly = true)
    public List<E> findAll() {
        List<E> result = new ArrayList<>();
        Iterable<E> items = repository.findAll();
        items.forEach(result::add);
        return result;
    }

    @Transactional(propagation = REQUIRED, readOnly = true)
    public E findById(UUID id) {
        E entity = findByIdNullable(id);
        if (entity == null) {
            throw new RuntimeException("Value not found");
        }
        return entity;
    }

    @Transactional(propagation = REQUIRED, readOnly = true)
    public E findByIdNullable(UUID id) {
        Optional<E> op = repository.findById(id);

        if (op.isEmpty()) {
            String canonicalName = repository.getEntityClass() != null ? repository.getEntityClass().getCanonicalName() : "undefined";
            log.error("Row with id: {} is not found in database. Class {}", id, canonicalName);
            return null;
        }

        return op.get();
    }

    @Transactional(propagation = REQUIRED)
    public void deleteById(UUID id) {
        repository.deleteById(id);
    }

    @Transactional(propagation = REQUIRED)
    public void delete(E entity) {
        repository.delete(entity);
    }

    @Transactional(propagation = REQUIRED)
    public void deleteAll(Iterable<? extends E> entities) {
        repository.deleteAll(entities);
    }

    @Transactional(propagation = REQUIRED)
    public E save(E entity) {
        return repository.save(entity);
    }

    @Transactional(propagation = REQUIRED)
    public void deleteAll() {
        repository.deleteAll();
    }

    @Transactional(propagation = REQUIRED)
    public Iterable<E> saveAll(Iterable<E> entities) {
        return repository.saveAll(entities);
    }

}