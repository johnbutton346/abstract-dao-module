package ru.isys.jpa.service;

import jakarta.persistence.criteria.Join;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.isys.jpa.dto.BaseFilterDto;
import ru.isys.jpa.dto.FilterType;
import ru.isys.jpa.dto.FiltersDto;
import ru.isys.jpa.dto.SearchRequestDto;
import ru.isys.jpa.dto.SingleFilterDto;
import ru.isys.jpa.dto.SortingDirection;

import java.util.ArrayList;
import java.util.List;

public class AbstractDao<E, R extends JpaSpecificationExecutor<E>> {


    private static final Logger log = LoggerFactory.getLogger(AbstractDao.class);

    protected R repository;

    @Autowired
    public AbstractDao(R repository) {
        this.repository = repository;
    }

    @Transactional
    public Page<E> findAll(Specification<E> specificationFilters, Pageable page) {
        return repository.findAll(specificationFilters, page);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<E> findAllByDtoAsList(SearchRequestDto dto) {
        return findAllBySpecAndDtoAsList(null, dto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public List<E> findAllBySpecAndDtoAsList(Specification<E> baseSpec, SearchRequestDto dto) {
        return findAllBySpecAndDto(baseSpec, dto).getContent();
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Page<E> findAllByDto(SearchRequestDto dto) {
        return findAllBySpecAndDto(null, dto);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Page<E> findAllBySpecAndDto(Specification<E> baseSpec, SearchRequestDto dto) {
        if (dto == null || dto.getPaging() == null) {
            return Page.empty();
        }

        Specification<E> specificationFilters = (baseSpec == null ? Specification.where(null) : baseSpec);

        if (dto.getFilters() != null) {
            specificationFilters = specificationFilters.and(processBaseFilters(dto.getFilters()));
        }

        Pageable pagingAndSorting;
        Sort sorting = null;

        if (dto.getSorting() != null) {
            sorting = Sort.by(dto.getSorting().getField());
            if (SortingDirection.valueOf(SortingDirection.DESC.name()).equals(dto.getSorting().getDirection())) {
                sorting = sorting.descending();
            }
        }

        if (sorting != null) {
            pagingAndSorting = PageRequest.of(dto.getPaging().getPage(), dto.getPaging().getSize(), sorting);
        } else {
            pagingAndSorting = PageRequest.of(dto.getPaging().getPage(), dto.getPaging().getSize());
        }

        return repository.findAll(specificationFilters, pagingAndSorting);
    }

    private Specification<E> processBaseFilters(BaseFilterDto base) {
        if (FilterType.simpleType(base.getType())) {
            return processSimpleFilter((SingleFilterDto) base);
        } else {
            log.debug("More than one column in filter: {}", base);
            List<BaseFilterDto> operands = ((FiltersDto) base).getOperands();

            List<Specification<E>> specifications = new ArrayList<>();

            for (BaseFilterDto operand : operands) {
                specifications.add(processBaseFilters(operand));
            }
            if (base.getType().equals(FilterType.OR)) {
                Specification<E> orSpecification = Specification.where(null);
                for (Specification<E> s : specifications) {
                    orSpecification = orSpecification.or(s);
                }
                return orSpecification;
            }
            Specification<E> andSpecification = Specification.where(null);
            for (Specification<E> s : specifications) {
                andSpecification = andSpecification.and(s);
            }
            return andSpecification;
        }
    }

    private Specification<E> processSimpleFilter(SingleFilterDto filter) {
        if (filter.getSearchMode() == null || filter.getSearchMode().equals(SingleFilterDto.SearchMode.SINGLE)) {
            return switch (filter.getType()) {
                case LIKE -> (root, query, criteriaBuilder) -> criteriaBuilder.like(
                        root.get(filter.getField()).as(String.class), likeFormat(filter.getValue())
                );
                case EQUALS -> (root, query, criteriaBuilder) -> criteriaBuilder.equal(
                        root.get(filter.getField()).as(String.class), filter.getValue()
                );
                case LIKE_IGNORE_CASE -> (root, query, criteriaBuilder) -> criteriaBuilder.like(
                        criteriaBuilder.lower(root.get(filter.getField()).as(String.class)), likeFormat(filter.getValue()).toLowerCase()
                );
                case EQUALS_IGNORE_CASE -> (root, query, criteriaBuilder) -> criteriaBuilder.equal(
                        criteriaBuilder.lower(root.get(filter.getField()).as(String.class)), filter.getValue().toLowerCase()
                );
                default -> Specification.where(null);
            };
        } else if (filter.getSearchMode().equals(SingleFilterDto.SearchMode.JOIN)) {
            return switch (filter.getType()) {
                case LIKE -> (root, query, criteriaBuilder) -> {
                    String[] fields = filter.getField().split("\\.");
                    Join<E, ?> responsibleJoin = root.join(fields[0]);
                    return criteriaBuilder.like(responsibleJoin.get(fields[1]).as(String.class), likeFormat(filter.getValue()));
                };
                case EQUALS -> (root, query, criteriaBuilder) -> {
                    String[] fields = filter.getField().split("\\.");
                    Join<E, ?> responsibleJoin = root.join(fields[0]);
                    return criteriaBuilder.equal(responsibleJoin.get(fields[1]).as(String.class), likeFormat(filter.getValue()));
                };
                case LIKE_IGNORE_CASE -> (root, query, criteriaBuilder) -> {
                    String[] fields = filter.getField().split("\\.");
                    Join<E, ?> responsibleJoin = root.join(fields[0]);
                    return criteriaBuilder.like(
                            criteriaBuilder.lower(responsibleJoin.get(fields[1]).as(String.class)), likeFormat(filter.getValue()).toLowerCase()
                    );
                };
                case EQUALS_IGNORE_CASE -> (root, query, criteriaBuilder) -> {
                    String[] fields = filter.getField().split("\\.");
                    Join<E, ?> responsibleJoin = root.join(fields[0]);
                    return criteriaBuilder.equal(
                            criteriaBuilder.lower(responsibleJoin.get(fields[1]).as(String.class)), likeFormat(filter.getValue()).toLowerCase()
                    );
                };
                default -> Specification.where(null);
            };
        }
        return Specification.where(null);
    }

    private static String likeFormat(String value) {
        return "%" + value + "%";
    }
}