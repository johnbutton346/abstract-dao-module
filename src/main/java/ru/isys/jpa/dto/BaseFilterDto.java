package ru.isys.jpa.dto;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = SingleFilterDto.class, name = "LIKE"),
        @JsonSubTypes.Type(value = SingleFilterDto.class, name = "LIKE_IGNORE_CASE"),
        @JsonSubTypes.Type(value = SingleFilterDto.class, name = "EQUALS"),
        @JsonSubTypes.Type(value = SingleFilterDto.class, name = "EQUALS_IGNORE_CASE"),
        @JsonSubTypes.Type(value = FiltersDto.class, name = "OR"),
        @JsonSubTypes.Type(value = FiltersDto.class, name = "AND")

})
public abstract class BaseFilterDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -3003371859801607594L;

    private FilterType type;
}
