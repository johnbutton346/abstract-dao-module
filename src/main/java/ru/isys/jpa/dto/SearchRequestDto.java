package ru.isys.jpa.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class SearchRequestDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -3523528040172124588L;

    private BaseFilterDto filters;

    private PagingDto paging;

    private SortingDto sorting;
}
