package ru.isys.jpa.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class SingleFilterDto extends BaseFilterDto implements Serializable {

    @Serial
    private static final long serialVersionUID = 5549563649173167947L;

    private String field;
    private String value;
    private SearchMode searchMode;

    public enum SearchMode {
        ALL,
        JOIN,
        SINGLE
    }
}
