package ru.isys.jpa.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class PagingDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -4585760808160332268L;

    private int page;

    private int size;
}
