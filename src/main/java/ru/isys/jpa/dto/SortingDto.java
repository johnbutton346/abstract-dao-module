package ru.isys.jpa.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;

@Getter
@Setter
public class SortingDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -4623672266792492160L;

    private String field;

    private SortingDirection direction;

}
