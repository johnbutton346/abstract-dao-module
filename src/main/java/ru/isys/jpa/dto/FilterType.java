package ru.isys.jpa.dto;

import lombok.Getter;

@Getter
public enum FilterType {

    OR,
    AND,
    LIKE,
    EQUALS,
    LIKE_IGNORE_CASE,
    EQUALS_IGNORE_CASE;

    public static boolean simpleType(FilterType filterType) {
        return !(filterType.equals(OR) || filterType.equals(AND));
    }
}
