package ru.isys.jpa.dto;

public enum SortingDirection {

    ASC,
    DESC
}
