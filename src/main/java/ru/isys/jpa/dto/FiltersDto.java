package ru.isys.jpa.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class FiltersDto extends BaseFilterDto implements Serializable {

    @Serial
    private static final long serialVersionUID = -8048166941897901678L;

    private List<BaseFilterDto> operands = new ArrayList<>();
}
